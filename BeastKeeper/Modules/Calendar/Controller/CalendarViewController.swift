//
//  CalendarViewController.swift
//  BeastKeeper
//
//  Created by Rafael Lellys on 2018-06-14.
//  Copyright © 2018 Rafa Lellys. All rights reserved.
//

import UIKit
import RealmSwift

enum Theme {
    case regular
}

class CalendarViewController: BaseViewController {
    
    @IBOutlet weak var availabilityView: UIView!
    
    
    @IBOutlet weak var saveButton: UIButton!
    
    var datePicked = String()
    var beastName = String()
    
    let calendarView: CalendarView = {
        let view = CalendarView(theme: Theme.regular)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    //MARK: - View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        calendarView.calendarDelegate = self
        
        availabilityView.addSubview(calendarView)
        
        calendarView.topAnchor.constraint(equalTo: availabilityView.topAnchor, constant: 0).isActive = true
        calendarView.rightAnchor.constraint(equalTo: availabilityView.rightAnchor, constant: 0).isActive = true
        calendarView.leftAnchor.constraint(equalTo: availabilityView.leftAnchor, constant: 0).isActive = true
        calendarView.heightAnchor.constraint(equalToConstant: 340).isActive = true
        calendarView.bottomAnchor.constraint(equalTo: availabilityView.bottomAnchor, constant: 0).isActive = true
        
        saveButton.titleLabel?.font = AppFonts.Medium.of(size: StandardSize.s2.rawValue)
        saveButton.adaptiveFont = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        showNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        calendarView.myCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    
    
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        
        //save date to realm
        let realm = try! Realm()
        let beast = realm.objects(MyBeast.self).filter("name = '\(beastName)'").first!
        
        try! realm.write {
            beast.vetDate = datePicked
              print(beast.vetDate)
        }
        
        dismissScreen()
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        
        dismissScreen()
    }
}

extension CalendarViewController : CalendarViewDelegate {
    
    func passDateSelected(datePassed: String) {
        datePicked = datePassed
    }
}

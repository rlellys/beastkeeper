//
//  MainViewController.swift
//  BeastKeeper
//
//  Created by Rafael Lellys on 2018-06-13.
//  Copyright © 2018 Rafa Lellys. All rights reserved.
//

import UIKit
import RealmSwift

class MainViewController: BaseViewController {
    
    @IBOutlet weak var myBeastsTableView: UITableView!
    @IBOutlet weak var noPetsView: UIView!
    @IBOutlet weak var findPetsTextLabel: UILabel!
    @IBOutlet weak var beastFinderCollectionView: UICollectionView!
    @IBOutlet weak var beastFinderCollectionViewFlowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var myBeastsLabel: UILabel!
    @IBOutlet weak var beastFinderLabel: UILabel!
    
    var allBeastsFromSource = Source.sharedInstance.allBeasts
    
    var realm =  RealmManager.shared.realm
    
    var myBeasts: Results<MyBeast> {
        get {
            return realm.objects(MyBeast.self)
        }
        
    }
    
    //MARK: - View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Main"
        
        myBeastsTableView.delegate = self
        myBeastsTableView.dataSource = self
        
        beastFinderCollectionView.delegate = self
        beastFinderCollectionView.dataSource = self
        
        myBeastsTableView.register(UINib(nibName: String(describing: MyBeastsTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: MyBeastsTableViewCell.self))
        
        beastFinderCollectionView.register(UINib.init(nibName: String(describing: BeastFinderCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: BeastFinderCollectionViewCell.self))
        
        beastFinderCollectionViewFlowLayout.estimatedItemSize = CGSize(width: 1, height: 1)
        
        myBeastsLabel.font = AppFonts.Medium.of(size: StandardSize.s3.rawValue)
        myBeastsLabel.adaptiveFont = true
        
        beastFinderLabel.font = AppFonts.Medium.of(size: StandardSize.s3.rawValue)
        beastFinderLabel.adaptiveFont = true
        
        //print("My beasts in Realm count: \(myBeasts.count)")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showNavigationBar()
        
        beastFinderCollectionView.reloadData()
        myBeastsTableView.reloadData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}

//MARK: - Collection View (All Beasts from source)


extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return allBeastsFromSource.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 200.0, height: 150.0)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = beastFinderCollectionView.dequeueReusableCell(withReuseIdentifier: String(describing: BeastFinderCollectionViewCell.self), for: indexPath) as! BeastFinderCollectionViewCell
        
        cell.beastImageView.image = allBeastsFromSource[indexPath.item].picture
        cell.beastNameLabel.text = allBeastsFromSource[indexPath.item].name
        cell.age = allBeastsFromSource[indexPath.item].age
        cell.type = allBeastsFromSource[indexPath.item].type
        cell.bloodType = allBeastsFromSource[indexPath.item].bloodType
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "BeastDetailViewController") as! BeastDetailViewController
        
        destination.beastName = allBeastsFromSource[indexPath.item].name
        destination.beastImage = allBeastsFromSource[indexPath.item].picture
        destination.isPushfromMyBeasts = false
        
        destination.beastAge = allBeastsFromSource[indexPath.item].age
        destination.beastType = allBeastsFromSource[indexPath.item].type
        destination.beastBloodType = allBeastsFromSource[indexPath.item].bloodType
        
        navigationController?.pushViewController(destination, animated: true)
        
    }
    
    
}

//MARK: - TableView (My Beasts)

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //check if we have ay beasts saved in Realm
        if myBeasts.count > 0 {
            
            noPetsView.isHidden = true
            
            return myBeasts.count
            
        } else  {
            
            //if no beasts saved, show message to go get beasts
            noPetsView.isHidden = false
            
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = myBeastsTableView.dequeueReusableCell(withIdentifier: String(describing: MyBeastsTableViewCell.self), for: indexPath) as! MyBeastsTableViewCell
        
        //load beasts saved from Realm
        let myBeast = myBeasts[indexPath.row]
        
        cell.nameLabel.text = myBeast.name
        
        //image in Realm is saved as Data so we convert it back to UIImage
        let picConverted = myBeast.picture
        let image = UIImage(data: picConverted!)
        cell.myBeastImageView.image = image
        
        cell.ageLabel.text = String(myBeast.age)
        cell.typeLabel.text = myBeast.type
        cell.bloodType = myBeast.bloodType
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "BeastDetailViewController") as! BeastDetailViewController
        
        destination.beastName = myBeasts[indexPath.row].name
        
        let picConverted = myBeasts[indexPath.row].picture
        guard let image = UIImage(data: picConverted!) else {return}
        
        destination.beastImage = image
        destination.beastAge = myBeasts[indexPath.row].age
        destination.beastType = myBeasts[indexPath.row].type
        destination.beastBloodType = myBeasts[indexPath.row].bloodType
        destination.isPushfromMyBeasts = true
        
        navigationController?.pushViewController(destination, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        let beastToBeRemoved = myBeasts[indexPath.row]
        
        if editingStyle == .delete {
            
            //remove my beast selected from Realm
            RealmManager.shared.delete(beastToBeRemoved)
            
            myBeastsTableView.deleteRows(at: [indexPath], with: .fade)
            
        }
    }
    
    
}

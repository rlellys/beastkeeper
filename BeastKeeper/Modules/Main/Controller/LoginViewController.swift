//
//  LoginViewController.swift
//  BeastKeeper
//
//  Created by Rafael Lellys on 2018-06-14.
//  Copyright © 2018 Rafa Lellys. All rights reserved.
//

import UIKit
import AVKit

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var loginBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackConstraint: NSLayoutConstraint!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var petLabel: UILabel!
    @IBOutlet weak var keeperLabel: UILabel!
    
    var player: AVPlayer?
    
    
    //MARK: - View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginButton.titleLabel?.font = AppFonts.Medium.of(size: StandardSize.s2.rawValue)
        loginButton.adaptiveFont = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideNavigationBar()
        playBackgroundVideo()
        addPlayerNotifications()
        
        //set initial position(before view appears) for login button and logo
        setupConstraintStartPoint(constraint: stackConstraint, newConstant: 200)
        setupConstraintStartPoint(constraint: loginBottomConstraint, newConstant: -80)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //set final position for login button and logo
        animateConstraint(constraint: stackConstraint, delay: 0, newConstant: 0)
        animateConstraint(constraint: loginBottomConstraint, delay: 0, newConstant: 0)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        removePlayerNotifications()
        player?.pause()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //MARK: - Animations
    
    
    func animateConstraint(constraint: NSLayoutConstraint, delay: TimeInterval, newConstant: CGFloat) {
        constraint.constant += self.view.bounds.height
        UIView.animate(withDuration: 2, delay: delay, usingSpringWithDamping: 0.8,initialSpringVelocity: 0.4,  options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func setupConstraintStartPoint(constraint: NSLayoutConstraint, newConstant: CGFloat) {
        //constraints start point before animating
        constraint.constant -= self.view.bounds.height
    }
    
    func playBackgroundVideo(){
        let url = URL(fileURLWithPath: Bundle.main.path(forResource: "cat_playback_trimmed", ofType: "mp4")!)
        player = AVQueuePlayer(playerItem: AVPlayerItem(url: url))
        player?.actionAtItemEnd = .none
        player?.isMuted = false
        
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = view.frame
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.zPosition = -1
        
        self.backgroundImageView.layer.addSublayer(playerLayer)
        
        player?.play()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(LoginViewController.loopVideo),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: nil)
    }
    
    @objc func loopVideo() {
        //go back to start point
        player?.seek(to: kCMTimeZero)
        player?.play()
    }
    
    func addPlayerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground), name: .UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground), name: .UIApplicationDidEnterBackground, object: nil)
    }
    
    func removePlayerNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIApplicationDidEnterBackground, object: nil)
    }
    
    
    @objc func applicationWillEnterForeground(_ notification: Notification) {
        //when app opens again, start video
        player?.play()
    }
    
    @objc func applicationDidEnterBackground(_ notification: Notification) {
        //when app goes to background, pause video
        player?.pause()
    }
    
    @IBAction func loginButtonTapped(_ sender: RoundedButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        navigationController?.pushViewController(destination, animated: true)
        
    }
    
    
}

//
//  BeastDetailViewController.swift
//  BeastKeeper
//
//  Created by Rafael Lellys on 2018-06-13.
//  Copyright © 2018 Rafa Lellys. All rights reserved.
//

import UIKit
import RealmSwift


class BeastDetailViewController: BaseViewController {
    
    @IBOutlet weak var beastDetailTableView: UITableView!
    
    @IBOutlet weak var keepBeastButton: RoundedButton!
    
    @IBOutlet weak var keepButtonBottomConstraint: NSLayoutConstraint!
    
    var beastName = String()
    var beastImage = UIImage()
    var beastAge = String()
    var beastType = String()
    var beastVetDate = String()
    var beastBloodType = String()
    
    @IBOutlet weak var beastDetailImageView: UIImageView!
    
    var myBeasts: Results<MyBeast> {
        get {
            return realm.objects(MyBeast.self)
        }
        
    }
    
    var realm: Realm!
    
    var isPushfromMyBeasts = Bool()
    
    let myBeast = MyBeast()
    
    struct beastDetailSections {
        
        static let profile = "Profile"
        static let nextVetDate = "Next vet appointment"
        static let bloodType = "Blood Type"
        static let count = 3
        
    }
    
    struct sourceBeastsSections {
        
        static let profile = "Profile"
        static let bloodType = "Blood Type"
        static let count = 2
        
    }
    
    struct beastProfileDetails {
        
        static let name = "Name"
        static let age = "Age"
        static let type = "Type"
        static let count = 3
    }
    
    //MARK: - View Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        realm = try! Realm()
        
        beastDetailTableView.delegate = self
        beastDetailTableView.dataSource = self
        
        beastDetailImageView.image = beastImage
        
        beastDetailTableView.register(UINib(nibName: String(describing: BeastDetailTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: BeastDetailTableViewCell.self))
        
        navigationItem.title = ""
        
        keepBeastButton.titleLabel?.font = AppFonts.Medium.of(size: StandardSize.s2.rawValue)
        keepBeastButton.adaptiveFont = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //when view appears, update data
        beastDetailTableView.reloadData()
        
        //if push came from My Beasts section
        if isPushfromMyBeasts {
            
            keepButtonBottomConstraint.constant = -50
            keepBeastButton.isHidden = true
            keepBeastButton.isEnabled = false
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Choose Vet Date", style: .plain, target: self, action: #selector(addTapped))
            
            //look for vet date information from Realm object
            do {
                realm = try Realm()
                //filter beast by name
                let beast = realm.objects(MyBeast.self).filter("name = '\(beastName)'").first!
                beastVetDate = beast.vetDate
            } catch let error as NSError {
                print(error)
            }
            
        //if push came from Beast Finder section
        } else {
            
            keepButtonBottomConstraint.constant = 10
            keepBeastButton.setTitle("Keep Beast", for: .normal)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func addTapped() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "CalendarViewController") as! CalendarViewController
        destination.beastName = beastName
        navigationController?.present(destination, animated: true, completion: nil)
        
    }
    
    @IBAction func keepBeastButtonTapped(_ sender: UIButton) {
        
        myBeast.name = beastName
        let data = UIImagePNGRepresentation(beastImage)
        myBeast.age = beastAge
        myBeast.picture = data
        myBeast.type = beastType
        myBeast.bloodType = beastBloodType
        
        //if it's a duplicate, only update beast object
        RealmManager.shared.update(myBeast)
        
        dismissScreen()
    }
    
}

//MARK: - TableView

extension BeastDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if isPushfromMyBeasts {
            
            return beastDetailSections.count
            
        } else {
            
            return sourceBeastsSections.count
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        switch section {
        case 0:
            
            return beastProfileDetails.count
            
        case 1:
            
            return 1
            
            
        default:
            
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        //if push came from My Beasts section

        if isPushfromMyBeasts {
            
            
            switch section {
            case 0:
                
                return beastDetailSections.profile
                
            case 1:
                
                return beastDetailSections.nextVetDate
                
            default:
                
                return beastDetailSections.bloodType
                
            }
            
        //if push came from Beast Finder section

        } else {
            
            switch section {
            case 0:
                
                return beastDetailSections.profile
                
            default:
                
                return beastDetailSections.bloodType
                
            }
            
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        guard let header = view as? UITableViewHeaderFooterView else { return }
        
        header.textLabel?.textColor = UIColor.lightGray
        header.textLabel?.font = AppFonts.Regular.of(size: StandardSize.s0.rawValue)
        header.textLabel?.adaptiveFont = true
        header.textLabel?.frame = header.frame
        header.textLabel?.textAlignment = .left
        header.backgroundView?.backgroundColor = .clear
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = beastDetailTableView.dequeueReusableCell(withIdentifier: String(describing: BeastDetailTableViewCell.self), for: indexPath) as! BeastDetailTableViewCell
        
        if isPushfromMyBeasts {
            
            
            switch indexPath.section {
                
            case 0:
                switch indexPath.row {
                case 0:
                    cell.beastDetailLabel.text = "Name: \(beastName)"
                case 1:
                    cell.beastDetailLabel.text = "Age: \(beastAge)"
                    
                default:
                    cell.beastDetailLabel.text = "Type: \(beastType)"
                }
                
            case 1:
                
                //fetch vet date hasn't been selected
                if beastVetDate != "" {
                cell.beastDetailLabel.text = "Date: \(beastVetDate)"
                } else {
                    cell.beastDetailLabel.text = "Date: Choose date"

                }
                
            default:
                cell.beastDetailLabel.text = "Blood Type: \(beastBloodType)"
            }
        } else {
            
            switch indexPath.section {
                
            case 0:
                switch indexPath.row {
                case 0:
                    cell.beastDetailLabel.text = "Name: \(beastName)"
                case 1:
                    cell.beastDetailLabel.text = "Age: \(beastAge)"
                    
                default:
                    cell.beastDetailLabel.text = "Type: \(beastType)"
                }
                
            default:
                cell.beastDetailLabel.text = "Blood Type: \(beastBloodType)"
            }
        }
        
        return cell
    }
    
}

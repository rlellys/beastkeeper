//
//  RealmManager.swift
//  BeastKeeper
//
//  Created by Rafael Lellys on 2018-06-15.
//  Copyright © 2018 Rafa Lellys. All rights reserved.
//

import Foundation
import RealmSwift

//Realm Manager

class RealmManager {
    private init(){}
    
    static let shared = RealmManager()
    
    var realm = try! Realm()
    
    func create<T: Object>(_ object: T) {
        do {
            try realm.write {
                
                realm.add(object)
                
            }
            
        } catch let error as NSError {
            print(error)
        }
        
    }
    
    func update<T: Object>(_ object: T) {
        
        do {
            try realm.write {
                //if it's a duplicate, only update beast object
                realm.add(object, update: true)
            }
        } catch let error as NSError {
            print(error)
        }
    }
    
    
    func delete<T: Object>(_ object: T) {
        do {
            try realm.write {
                
                realm.delete(object)
                
            }
            
        } catch let error as NSError {
            print(error)
        }
        
    }
    
    
}

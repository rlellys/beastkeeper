//
//  MyBeast.swift
//  BeastKeeper
//
//  Created by Rafael Lellys on 2018-06-13.
//  Copyright © 2018 Rafa Lellys. All rights reserved.
//

import Foundation
import RealmSwift

//Realm object

class MyBeast: Object {
    
    @objc dynamic var name = ""
    @objc dynamic var age = ""
    @objc dynamic var picture: Data? = nil
    @objc dynamic var type = ""
    @objc dynamic var bloodType = ""
    @objc dynamic var vetDate = ""
    
    override class func primaryKey() -> String? {
        return "name"
    }
}

//
//  AllBeasts.swift
//  BeastKeeper
//
//  Created by Rafael Lellys on 2018-06-13.
//  Copyright © 2018 Rafa Lellys. All rights reserved.
//

import Foundation
import RealmSwift

class AllBeastsInRealm: Object {
    
    @objc dynamic var name = ""
    @objc dynamic var age = 0
    @objc dynamic var picture: Data? = nil
    
}

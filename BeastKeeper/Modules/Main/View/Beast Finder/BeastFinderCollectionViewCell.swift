//
//  BeastFinderCollectionViewCell.swift
//  BeastKeeper
//
//  Created by Rafael Lellys on 2018-06-13.
//  Copyright © 2018 Rafa Lellys. All rights reserved.
//

import UIKit

class BeastFinderCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var beastImageView: UIImageView!
    @IBOutlet weak var beastNameLabel: UILabel!
    
    var age = String()
    var type = String()
    var bloodType = String()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        beastNameLabel.font = AppFonts.Medium.of(size: StandardSize.s2.rawValue)
        beastNameLabel.adaptiveFont = true
        
    }
}

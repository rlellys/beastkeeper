//
//  MyBeastsTableViewCell.swift
//  BeastKeeper
//
//  Created by Rafael Lellys on 2018-06-14.
//  Copyright © 2018 Rafa Lellys. All rights reserved.
//

import UIKit

class MyBeastsTableViewCell: UITableViewCell {

    @IBOutlet weak var myBeastImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    
    var bloodType = String()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        nameLabel.font = AppFonts.Medium.of(size: StandardSize.s2.rawValue)
        nameLabel.adaptiveFont = true
        
        ageLabel.font = AppFonts.Medium.of(size: StandardSize.s2.rawValue)
        ageLabel.adaptiveFont = true
        
        typeLabel.font = AppFonts.Medium.of(size: StandardSize.s2.rawValue)
        typeLabel.adaptiveFont = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

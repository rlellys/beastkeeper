//
//  BeastDetailTableViewCell.swift
//  BeastKeeper
//
//  Created by Rafael Lellys on 2018-06-13.
//  Copyright © 2018 Rafa Lellys. All rights reserved.
//

import UIKit

class BeastDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var beastDetailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        beastDetailLabel.font = AppFonts.Medium.of(size: StandardSize.s1.rawValue)
        beastDetailLabel.adaptiveFont = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

//
//  Fonts.swift
//  BeastKeeper
//
//  Created by Rafael Lellys on 2018-06-13.
//  Copyright © 2018 Rafa Lellys. All rights reserved.
//

import UIKit


enum AppFonts: String {
    case Regular = "Futura"
    case Medium = "Futura-Medium"
    case SemiBold = "Futura-Bold"
    
    func of(size: CGFloat) -> UIFont? {
        return UIFont(name: self.rawValue, size: size)
    }
}

enum StandardSize: CGFloat {
    case s0 = 12.0
    case s1 = 13.0
    case s2 = 14.0
    case s3 = 16.0
    case s4 = 17.0
    case s5 = 20.0
    case s6 = 25.0
    
}


let fontSetForWidth : CGFloat = 375.0


@IBDesignable extension UILabel
{
    @IBInspectable var adaptiveFont:Bool
        {
        set
        {
            if newValue == true
            {
                let fontDescriptor = self.font.fontDescriptor
                let currentFontSize = fontDescriptor.pointSize
                let newFontSize = (currentFontSize/fontSetForWidth)*UIScreen.main.bounds.size.width
                let newFont = UIFont(descriptor: fontDescriptor, size: newFontSize)
                font = nil
                font = newFont
            }
        }
        get
        {
            return false
        }
    }
}
@IBDesignable extension UIButton
{
    @IBInspectable var adaptiveFont:Bool
        {
        set
        {
            if newValue == true
            {
                let fontDescriptor = self.titleLabel!.font.fontDescriptor
                let currentFontSize = fontDescriptor.pointSize
                let newFontSize = (currentFontSize/fontSetForWidth)*UIScreen.main.bounds.size.width
                let newFont = UIFont(descriptor: fontDescriptor, size: newFontSize)
                titleLabel!.font = nil
                titleLabel!.font = newFont
            }
        }
        get
        {
            return false
        }
    }
}

@IBDesignable extension UITextField
{
    @IBInspectable var adaptiveFont:Bool
        {
        set
        {
            if newValue == true
            {
                let fontDescriptor = self.font!.fontDescriptor
                let currentFontSize = fontDescriptor.pointSize
                let newFontSize = (currentFontSize/fontSetForWidth)*UIScreen.main.bounds.size.width
                let newFont = UIFont(descriptor: fontDescriptor, size: newFontSize)
                font = nil
                font = newFont
            }
        }
        get
        {
            return false
        }
    }
}

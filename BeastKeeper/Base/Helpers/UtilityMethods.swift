//
//  UtilityMethods.swift
//  BeastKeeper
//
//  Created by Rafael Lellys on 2018-06-13.
//  Copyright © 2018 Rafa Lellys. All rights reserved.
//

import UIKit

class UtilityMethods {
    
    //MARK:- Set shadow on view
    
    class func setShadowOnView(_ view : UIView) {
        
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowRadius = 4
        view.layer.shadowOffset = CGSize(width: 1.0, height: 3.0)
    }
    
}

//
//  UIColorExtension.swift
//  BeastKeeper
//
//  Created by Rafael Lellys on 2018-06-15.
//  Copyright © 2018 Rafa Lellys. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var darkPurple: UIColor {
        return UIColor(red: 114/255, green: 26/255, blue: 185/255, alpha: 1)
    }
    
    static var darkOrange: UIColor {
        return UIColor(red: 243/255, green: 127/255, blue: 43/255, alpha: 1)
    }
    
    static var darkText: UIColor {
        return UIColor(red: 29/255, green: 29/255, blue: 38/255, alpha: 1)
    }
    
    static var fadedText: UIColor {
        return UIColor(red: 29/255, green: 29/255, blue: 38/255, alpha: 0.5)
    }
    
    
    
    
}

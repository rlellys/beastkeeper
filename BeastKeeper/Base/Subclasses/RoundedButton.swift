//
//  RoundedButton.swift
//  BeastKeeper
//
//  Created by Rafael Lellys on 2018-06-14.
//  Copyright © 2018 Rafa Lellys. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {
    
    override func awakeFromNib() {
        
        layer.cornerRadius = 5
        UtilityMethods.setShadowOnView(self)
    }
    
    
}

//
//  GradientView.swift
//  BeastKeeper
//
//  Created by Rafael Lellys on 2018-06-14.
//  Copyright © 2018 Rafa Lellys. All rights reserved.
//

import UIKit

class GradientView: UIView {
    
    let purpleColourTop : UIColor = .darkPurple
    let purpleColourBottom : UIColor = .darkGray
    
    override open class var layerClass: AnyClass {
        return CAGradientLayer.classForCoder()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let gradientLayer = layer as! CAGradientLayer
        gradientLayer.colors = [purpleColourTop.cgColor,purpleColourBottom.cgColor]
        gradientLayer.locations = [0.2,0.8]
    }
}

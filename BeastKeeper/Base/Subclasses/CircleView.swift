//
//  CircleView.swift
//  BeastKeeper
//
//  Created by Rafael Lellys on 2018-06-14.
//  Copyright © 2018 Rafa Lellys. All rights reserved.
//

import UIKit

class CircleView: UIView {
    
    override func awakeFromNib() {
        
        layer.cornerRadius = frame.size.height * 0.5
        layer.borderColor = UIColor.purple.cgColor
        layer.borderWidth = 0.5
        clipsToBounds = true
    }
    
}

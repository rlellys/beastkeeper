//
//  Beast.swift
//  BeastKeeper
//
//  Created by Rafael Lellys on 2018-06-13.
//  Copyright © 2018 Rafa Lellys. All rights reserved.
//

import Foundation
import RealmSwift

class Beast {
    
    var name = String()
    var age = String()
    var type = String()
    var bloodType = String()
    var picture = UIImage()
    
    init(name: String, age: String,type: String, bloodType:String, picture: UIImage) {

        self.name = name
        self.age = age
        self.type = type
        self.bloodType = bloodType
        self.picture = picture
    }
    
}


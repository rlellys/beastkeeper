//
//  Source.swift
//  BeastKeeper
//
//  Created by Rafael Lellys on 2018-06-13.
//  Copyright © 2018 Rafa Lellys. All rights reserved.
//

import UIKit


struct animalTypes {
    
    static let dog = "Dog"
    static let cat = "Cat"
    static let bird = "Bird"
    static let turtle = "Turtle"

}

struct bloodTypes {
    
    static let oPlus = "O+"
    static let oNeg = "O-"
    static let aPlus = "A+"
    static let ab = "AB"
    
}

//MARK: - Source

class Source {
    
    var beast1 = Beast(name: "Ella", age: "2", type: animalTypes.dog  , bloodType: bloodTypes.ab ,  picture: #imageLiteral(resourceName: "ella2"))
    var beast2 = Beast(name: "Fido", age: "5", type: animalTypes.dog  , bloodType: bloodTypes.oNeg, picture: #imageLiteral(resourceName: "dog2"))
    var beast3 = Beast(name: "Scooby", age: "13", type: animalTypes.dog  , bloodType: bloodTypes.oPlus, picture: #imageLiteral(resourceName: "dog1"))
    var beast4 = Beast(name: "Snoopy", age: "10",type: animalTypes.dog  , bloodType: bloodTypes.aPlus, picture: #imageLiteral(resourceName: "dog4"))
    var beast5 = Beast(name: "Bidu", age: "4" ,type: animalTypes.bird  , bloodType: bloodTypes.oPlus, picture: #imageLiteral(resourceName: "bird1"))
    var beast6 = Beast(name: "Rex", age: "6" ,type: animalTypes.cat  , bloodType: bloodTypes.ab, picture: #imageLiteral(resourceName: "cat1"))
    var beast7 = Beast(name: "Nico", age: "11" ,type: animalTypes.cat  , bloodType: bloodTypes.aPlus, picture: #imageLiteral(resourceName: "cat2"))
    var beast8 = Beast(name: "Kika", age: "2" ,type: animalTypes.turtle  , bloodType: bloodTypes.oNeg, picture: #imageLiteral(resourceName: "turtle1"))
    var beast9 = Beast(name: "Jade", age: "17" ,type: animalTypes.turtle  , bloodType: bloodTypes.oNeg, picture: #imageLiteral(resourceName: "turtle3"))
    var beast10 = Beast(name: "Lulu", age: "7" ,type: animalTypes.bird  , bloodType: bloodTypes.aPlus, picture: #imageLiteral(resourceName: "bird2"))
    
    lazy var allBeasts = [beast1, beast2, beast3, beast4, beast5, beast6, beast7, beast8, beast9, beast10]
    
    //MARK: - Singleton
    static let sharedInstance = Source()
    private init() {}
    
    
}
